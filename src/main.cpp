#include <iostream>
#include "screencapture.h"

using namespace std;

int main() {
    cout << "Hello, World!" << endl;

    // Make a screenie
    screencapture capture;
    capture.capture_desktop();

    return 0;
}