//
// Created by Bart on 4/26/2015.
//

#include "screencapture.h"
#include "CImg.h"
using namespace cimg_library;

screencapture::screencapture() {

}

void screencapture::capture_desktop() {
    // Try all the options
    x11_screenshot();
}

void screencapture::x11_screenshot() {
#ifdef __linux__

    Display *display = XOpenDisplay(NULL);
    //Display *display2 = XOpenDisplay(":0");
    Window root = DefaultRootWindow(display);
    XWindowAttributes gwa;

       XGetWindowAttributes(display, root, &gwa);
       int width = gwa.width;
       int height = gwa.height;

       XImage *image = XGetImage(display,root, 0,0 , width,height,AllPlanes, ZPixmap);

       unsigned long red_mask = image->red_mask;
   unsigned long green_mask = image->green_mask;
   unsigned long blue_mask = image->blue_mask;

    unsigned char *array = new unsigned char[width * height * 3];
    CImg<unsigned char> pic(array,width,height,1,3);

    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height ; y++) {
         unsigned long pixel = XGetPixel(image,x,y);

         unsigned char blue = pixel & blue_mask;
         unsigned char green = (pixel & green_mask) >> 8;
         unsigned char red = (pixel & red_mask) >> 16;

         /*array[(x + width * y) * 3] = red;
         array[(x + width * y) * 3+1] = green;
         array[(x + width * y) * 3+2] = blue;*/
         pic(x,y,0) = red;
         pic(x,y,1) = green;
         pic(x,y,2) = blue;
      }
    }

    pic.save_png("blah.png");
#endif
}

void screencapture::win32_screenshot() {
#ifdef _WIN32

#endif
}

void screencapture::osx_screenshot() {
#ifdef __APPLE__

#endif
}
