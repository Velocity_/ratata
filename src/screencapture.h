//
// Created by Bart on 4/26/2015.
//

#ifndef RATATA_SCREENCAPTURE_H
#define RATATA_SCREENCAPTURE_H

#include <cstdio>
#include <iostream>

// Linux-only X11 headers
#ifdef __linux__
#include <X11/Xlib.h>
#include <X11/X.h>
#endif

class screencapture {

public:
    screencapture();
    void capture_desktop();

private:
    void x11_screenshot();
    void win32_screenshot();
    void osx_screenshot();
};


#endif //RATATA_SCREENCAPTURE_H
